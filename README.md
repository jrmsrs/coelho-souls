# Coelho Souls

![GitLab repo size](https://img.shields.io/gitlab/repo-size/jrmsrs/coelho-souls?style=for-the-badge)
![GitLab language count](https://img.shields.io/gitlab/languages/count/jrmsrs/coelho-souls?style=for-the-badge)
![GitLab forks](https://img.shields.io/gitlab/forks/jrmsrs/coelho-souls?style=for-the-badge)

[Play on browser](https://jrmsrs.itch.io/coelhosouls) (password: jrmsrs)

## 📄 Game Design Document

[GDD.md](GDD.md)  

## 📝 License

[MIT](LICENSE)