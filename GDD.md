> GDD Template Written by: Benjamin “HeadClot” Stanley (adaptado)
# Índice

- [Overview](#overview)
  - [Tema / Gênero](#tema--gênero)
  - [Mecânicas de Gameplay (Resumo)](#mecânicas-de-gameplay-resumo)
  - [Plataformas](#plataformas)
  - [Controles](#controles)
  - [Escopo do Projeto](#escopo-do-projeto)
  - [Influências da mídia](#influências-da-mídia)
  
- [Narrativa, Estética e Gameplay](#narrativa-estética-e-gameplay)
  
  - [Narrativa](#narrativa)
  - [Estética](#estética)
  - [Gameplay (Resumo)](#gameplay-resumo)
  - [Gameplay (Detalhado)](#gameplay-detalhado)
  
- [O projeto](#o-projeto)
  - [Elementos do jogo](#elementos-do-jogo)
  - [Elementos do jogo: Personagens (Creature Object)](#elementos-do-jogo-personagens-creature-object)
  - [Elementos do jogo: Objetos de nível (Level Object)](#elementos-do-jogo-objetos-de-nível-level-object)
  - [Elementos do jogo: Mecânicas](#elementos-do-jogo-mecânicas)
  - [Level Design](#level-design)
  
- [Assets](#assets)
  - [Artes](#artes)
  - [Sons](#sons)
  - [Animações](#animações)
  
- [Schedule](#schedule)

- [Referências](#referências)

  <div style="page-break-after: always; break-after: page;"></div>

# Overview

## Tema / Gênero

Coelho Souls é um jogo no estilo action-platformer/metroidvania. O jogador inicia com mecânicas limitadas e obtém upgrades que possibilitam explorar ainda mais o mapa, para melhoram seus atributos, desbloqueiam habilidades para combate ou mobilidade, etc.

## Mecânicas de Gameplay (Resumo)

  As mecânicas de game play principais do jogo segue o que alguns chamam de "unlock loop" de metroidvanias, que é baseado na recompensa por explorar, você repetidamente vai buscar por power ups, que desbloqueiam areas antes inacessíveis, para buscar mais power ups. Então basicamente o jogo é centrado na mecânica de power up.
  Portanto isso unido a características de plataforma e ação baseia-se principalmente em: mover, pular, mirar, derrotar inimigos, coletar power-ups, acessar novos locais.


## Plataformas
  - **HTML5** (PC, consoles que suportam browser, mobile - apenas com controle) em plataforma como [itch.io](itch.io).
  - **Windows**
  - **Linux**

## Controles
| **Teclado/Mouse** |      **Ação**     |
|:-----------------:|:-----------------:|
|        A/D        |    Movimentação   |
|         W         |     Pulo/Cima     |
|         S         |       Baixo       |
|      MB_LEFT      |      Ataque1      |
|      MB_RIGHT     | Ataque2 (upgrade) |
|       SPACE       |   Dash (upgrade)  |
|         E         |        Ação       |

## Escopo do projeto
  - Game Time Scale
    - Escala de tempo: até 02/08/2022
  - Equipe
    - Junior: coding, game design, pixel art

## Influências da mídia
  - Hollow Knight (videogame)
    - As mecânicas principais do jogo
  - A Revolução dos Bichos - George Orwell (literatura)
    - Narrativa em que dá consciência a animais pro bem e pro mal, tema de fazenda.
<center style="zoom:17%;">
    <img src="https://images.saymedia-content.com/.image/ar_4:3%2Cc_fill%2Ccs_srgb%2Cfl_progressive%2Cq_auto:eco%2Cw_1200/MTc0NDQxMDkzNDI3ODk3OTkw/animal-farm-a-book-report.jpg" style="zoom:120%;" /> <img src="https://conteudo.imguol.com.br/c/entretenimento/be/2018/07/17/hollow-knight-contra-chefe-1531857733346_v2_4x3.jpg" />
</center>

  - Terraria (videogame)
    - Inspiração de algumas IA e estética de criaturas, jogo 2D em que usa mouse para mirar e atacar, mistura sutil entre pixel art e HD.
  - Minecraft (videogame)
    - Inspiração de algumas IA e estética de criaturas, trilha sonora, mistura entre pixel art e 3D.
<center style="zoom:29%;">
    <img src="https://www.minecraft.net/content/dam/games/minecraft/key-art/Xbox_Minecraft_WildUpdate_Main_.Net_600x360.png" style="zoom:140%;" /> <img src="https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/wiiu_14/SI_WiiU_3DS_Terraria.jpg" />
</center>


  - Jogos de Gameboy em geral
    - Estética, paleta de cores, baixa resolução
<center style="zoom:11%;" >
    <img src="https://images.igdb.com/igdb/image/upload/t_screenshot_huge_2x/v0j2acorykqklqnbxrmy.jpg" /> <img src="https://external-preview.redd.it/ZeW3nZKAnQs13p0ExC3ooo4km6-as-F3kBlhppAIpgM.jpg?auto=webp&s=6613afc925ecb7c62060713dc9fb5468b32b5908" style="zoom:500%;" />
</center>

<div style="page-break-after: always; break-after: page;"></div>

# Narrativa, Estética e Gameplay

## Narrativa

Num fantasioso mundo de animais humanoides, há um coelho numa vila em que planta suas próprias cenouras, até surgir uma praga, a toupeira Dr. Molenksy, esse que é um cientista maluco que vive de fazer experimentos bizarros.  
O coelho será jogado num calabouço, cavado pelo próprio Molenksy, dentro do poço de sua fazenda onde descobrirá os seus experimentos e deverá combater caso queira sobreviver.

## Estética

O jogo adota pixel arts primariamente de baixíssimas resoluções remetendo a consoles portáteis antigos, como blocos de 8x8, assim como várias "paleta de cores" (na verdade um blending) em áreas diferentes, remetendo a vibe de Gameboy, mas ainda que utiliza uma técnica de "fake" pixel art, ou seja, os assets são escalonados para resoluções tradicionais como 1920x1080 que permite um efeito de por exemplo rotacionar sprites sem distorcer, e na verdade pretende abusar de rotacionar e escalar as sprites em números que não fariam sentidos nas displays desses consoles antigos, além de não dispor as sprites em "pixel-perfect snap".

<img src="gddimg/sunn.png" style="zoom:50%;" />

Como ambientação sonora o jogo busca trazer uma trilha bem calma e ambiental, similar a trilha de Minecraft. Com certo esforço poderia encontrar algumas musicas do tema pela internet. Assim como SFX em sites como [freesound.org](https://freesound.org/) e [opengameart.org](https://opengameart.org/).

<img src="gddimg/sunnn.png" style="zoom:50%;" />


## Gameplay (Resumo)

Trata-se de um jogo 2D em que o jogador, após o “tutorial” se encontrará numa profundeza, cavada pelo antagonista, e tem o objetivo de retornar a superfície onde vai ter um confronto final.

Para isso, o jogador deve consertar um elevador, que funciona mecanicamente desde que tenha algo para rodar as engrenagens, o que pode ser obtido salvando os Rattos, que são os ratos do laboratório do antagonista – que tudo o que fazem é correr em rodas ligadas as engrenagens sem parar, a progressão no jogo (que é o limite de altura que o elevador funciona) crescerá ao mesmo tempo em que o jogador obtém as 4 principais habilidades que desbloqueiam novas mecânicas de movimentação pelo mapa.

## Gameplay (Detalhado)
### Parte 1. Tutorial
O jogador inicialmente deve realizar as primeiras tarefas como um tutorial, primeiramente na superficie em que ensina movimentos mais básicos como mover para o lado e pro outro, pular e "atacar" que inicialmente é uma pá para obter as primeiras cenouras, depois de obter as primeiras cenouras e tentar retornar a casa o jogador será lançado dentro do poço.

A partir daí a segunda parte do tutorial ensina pulos mais complicados e combate, introduz o primeiro checkpoint e o primeiro inimigo, toda vez que o jogador usar o checkpoint, os inimigos irão renascer. Após caminhar mais o jogador verá créditos que marca o fim do tutorial, no momento que o jogador tentar fugir do poço subindo a corda ele será lançado para ainda mais pro fundo do que da ultima vez e será o momento em que o jogo realmente começa.

### Parte 2. O jogo
O jogador será introduzido ao elevador ainda não funcional e deve explorar o resto do mapa sendo a Área A a primeira, representada pelo tema de cores e a trilha sonora, e pode transitar para a Area 2 livremente, onde se encontra, indo a direita superior, até onde a moving platform levará, o primeiro upgrade possível de ser obtido, o dash, e o primeiro Ratto a ser salvo, necessitando do uso do dash logo após pular para alcançar este local.

Ao obter o dash um local antes inacessível na Area 1 estará acessível, nesse local se encontrará o próximo upgrade, e o segundo Ratto, esse que pode fazer o elevador chegar até o segundo nível, que é apenas um atalho para a Área 2.
Assim o jogo prosseguirá, tendendo a dificultar quanto mais próximo esteja dos power ups, sendo que nos ultimos locais, o 3º e o 4º, haverá minibosses, basicamente uma versão maior, mais rápida, e com maior resistência e portanto mais dificil dos inimigos comuns.

### Parte 3. Final
Após o jogador coletar 4 Rattos, o elevador poderá finalmente ir até o primeiro nível, onde o jogador pode alcançar novamente a área do tutorial, porém ao retornar a superficie terá o confronto com o boss final.

<div style="page-break-after: always; break-after: page;"></div>

# O projeto

## Elementos do jogo

Os objetos do jogo seguem uma forma de herança de classes por questão de escalabilidade do projeto. Sendo os que estão em verde do tipo Entity, que portanto não podem ser instanciados.

![res://gddimg/classes.png](gddimg/classes.png)

## Elementos do jogo: Personagens (Creature Object)
  - **Player**  
    <center><img src="gddimg/playzinho.png" />  </center>
    Personagem principal, um coelho fazendeiro, inicialmente com uma pá para colher cenouras, mas que logo no início do jogo obtém um chicote como arma.  
    Entity: Creature, Grounded  
    Vida inicial: 8
  - **Piggo**  
    <center><img src="gddimg/apiggo.png" />  </center>
    Vizinho do Player na vila.  
    Entity: Creature, Grounded, Npc  
    Vida inicial: undef
  - **Cowu**  
    <center><img src="gddimg/acow.png" />  </center>
    Vizinho do Player na vila, precisa ser resgatado.  
    Entity: Creature, Grounded, Npc  
    Vida inicial: undef
  - **Ratto**  
    <center><img src="gddimg/arat.png" />  </center>
    NPCs que trabalham apenas para girar as engrenagens de mecanismos pelo mapa.  
    Entity: Creature, Grounded, Npc  
    Vida inicial: undef
  - **Crabz**  
    <center><img src="gddimg/acrab.png" />  </center>
    Inimigos comuns que patrulham e seguem na direção do jogador até onde pode se movimentar e em certa área de alcance, mutações permitem uma mecânica de pulo.  
    Entity: Creature, Grounded, Enemy  
    Vida inicial: 2
  - **Baty**  
    <center><img src="gddimg/abat.png" />  </center>
    Inimigos comuns que voam e perseguem o jogador até certa área de alcance.  
    Entity: Creature, FlyingEnemy  
    Vida inicial: 2
  - **Squir**  
    
    <center><img src="gddimg/asquir.png" />  </center>
    Inimigos comuns que patrulham e seguem na direção do jogador até estar em sua área de alcance, então ele lançará nozes (BombNut) que explodem depois de um tempo, e ao tocar no Player esse tempo é encurtado. O jogador pode rebater as BombNuts para longe dele, mas isso causa dano apenas ao Player.  
    Entity: Creature, Grounded, Enemy  
    Vida inicial: 3
  - **DrMolensky**  
    
    <center><img src="gddimg/amoler.png" />  </center>
    
    Boss final  
    Entity: Creature, Grounded, Boss  
    Vida inicial: a definir

  - outros
    - **mutações**  
    Alguns inimigos possuem mutações, na resistencia, na vida, velocidade, tamanho, etc e inclusive algumas vezes na IA.  
    <center><img src="gddimg/um.png" alt="gddimg/um.png" style="zoom:67%;" />  </center>
    <i>Crabz pulando</i>
    - **minibosses**  
    Também são considerados mutações, porém mais expecíficos e apenas um por área, costumam estrapolar na resistência, velocidade, etc, para fornecer um desafio considerável ao jogador. 
    <center><img src="gddimg/treisu.png" alt="gddimg/treisu.png" style="zoom:67%;" />  </center>
    <i>Miniboss "Squirr"</i>  
    
    <center><img src="gddimg/doisu.png" alt="gddimg/doisu.png" style="zoom:67%;" />  </center>
    <center><i>Miniboss "Crabzy"</i> </center>

## Elementos do jogo: Objetos de nível (Level Object)
  - **Spike**  
    
    <center><img src="gddimg/spikes.png" alt="gddimg/spikes.png" style="zoom:50%;" />  </center>
    Elemento que o jogador ao colidir terá subtraido 1 ponto de sua vida e fará com que ele retorne ao inicio da room (por onde entrou), causa instakill em inimigos Grounded.
  - **MovingPlatform**  
    
    <center><img src="gddimg/movplat.png" alt="gddimg/spikes.png" style="zoom:50%;" />  </center>
    Tiles sólidos que estão sempre se movendo para cima e para baixo, é programado para inicialmente ir na direção Y do player e resetar caso esteja muito distante, fazendo com que chegue um pouco mais rápido, para que o player não tenha que esperar muito tempo.
  - **DestroyableBlock**  
    
    <center><img src="gddimg/destblock.png" alt="gddimg/spikes.png" style="zoom:50%;" />  </center>
    Tiles sólidos que podem ser destruídos.


## Elementos do jogo: Mecânicas
  - **Mover**  
    O jogador consegue se mover pra esquerda ou direita.
  - **Pular/Cair**  
    O jogador consegue pular quando está no chão e sofre ação da gravidade que faz com que ele caia quando não está colidindo com um chão.
  - **Ataque primário**  
    O jogador ataca na direção do cursor, é possível usar o ataque primário enquanto está nos estados: Idle, Move, Jump e Fall.
  - **Mover câmera**  
    O cursor (mouse/gamepad Raxis) permite movimentar a câmera até uma área delimitada.
  - **Resgatar NPCs**  
    Alguns NPCs serão resgatados ao colidir com eles.
  - **Checkpoint**  
    O jogador ao colidir com o checkpoint e aguardar um momento irá descansar, sua vida será restaurada e os Carrots serão restaurado até o limite de 5 caso tenha menos que isso, isso fará o jogo reiniciar na progressão em que o jogador está, respawnando todos os inimigos.
  - **Elevador**  
    Em um elevador ao pressionar cima ou baixo dependendo do nível do habilitado do elevador fará com que o player se teletransporte e o player use o checkpoint atrelado a ele, causando os mesmos acontecimentos relacionados a mecânica de checkpoint. O nível do elevador depende da quantidade de Rattos resgatados, a cada 2, um nível será desbloqueado.
  - **Autoconsumíveis**  
    O jogador ao obter itens como Life e Carrot irá aumentar até um limite os respectivos atributos.
  - **Autoconsumível/Carrot**  
    Aumenta o contador de Carrots e pode ser estocado até o máximo de 12.
  - **Autoconsumível/Life**  
    Aumenta 1 ponto de vida (meio cristal exibido na GUI)
  - **Upgrades**  
    O jogador ao obter Orbs ou algum item específico irá desbloquear outras mecânicas.
  - **Upgrade/Ataque secundário**  
    Após desbloquear o jogador poderá lançar projéteis Carrots para a direção do cursor. É possível usar o ataque secundário apenas quando está no estado Idle.
  - **Upgrade/Dash**  
    O jogador após desbloquear essa habilidade caso esteja movendo o personagem no momento (tecla de Dash + left ou right) atinge uma alta velocidade constante por breves milissegundos, e caso esteja no ar que também irá anular a ação da gravidade, se o jogador usar dash e pular antes do efeito acabar, o dash vai ser cancelado enquanto esse pulo terá um boost de velocidade, como em alguns jogos da série Megaman. Se o jogador usar Dash na terra, sem pressionar left/right, ele irá desviar com um pulo para trás.
  - **Upgrade/Explosive Carrot**  
    É assumido que o jogador já desbloqueou o ataque secundário, então as Carrots agora explodirá ao encostar em objetos, permitindo que destrua blocos quebráveis, além de ter um upgrade na velocidade, no alcance do ataque e dobrar o dano quando acertado em cheio (quando acerta um inimigo causa X dano + X quando explode, mas caso apenas a explosão acerte o inimigo, ele sofrerá apenas a quantidade X de dano como usual).
  - **Upgrade/Wall Jump**  
    O jogador consegue deslizar lentamente pela parede e pular sobre elas enquanto está nesse estado.
  - **Upgrade/Double Jump**  
    O jogador pode pular (uma vez) enquanto está no ar -- estado "Jump" ou "Fall", o que permite alcançar locais mais altos. 
  - **Ataque primário por cima**  
    Uma mecânica adicional de ataque, se o ataque colidir por cima de um inimigo (o jogador precisa estar no ar) fará com que o jogador ganhe um impulso para cima, permitindo mais possibilidades de combate, o mesmo acontece com Spikes, que dá a possibilidade de "caminhar" sob os espinhos.

## Implementação de State Machines
  Devido ao tamanho do projeto é quase obrigatório por questão de escalabilidade a implementação de SM, que irá facilitar o trabalho para desenvolver. Abaixo um exemplo de implementação algumas mecânicas de um protótipo de "Player" na forma mais simples de State Machines:

  ```python
  enum state { IDLE, MOVE, AIR }
  var dir_x: int = 0
  var current_state: int = state.IDLE

  def main_loop() -> void:
    dir_x = get_gamepad_left_axis().x
    match current_state:
      case IDLE: 
        idle_state()
      case MOVE: 
        move_state()
      case AIR:  
        air_state()

  def idle_state() -> void:
    vel.x = 0
    if dir_x!=0: current_state=state.MOVE
    if vel.y!=0: current_state=state.AIR
    apply_gravity()
  
  def move_state() -> void:
    if dir_x==0: current_state=state.IDLE
    if vel.y!=0: current_state=state.AIR
    apply_movement()
    apply_gravity()

  def air_state() -> void:
    if vel.y!=0: current_state=state.IDLE
    apply_movement()
    apply_gravity()

  (...)
  ```

  O uso de FSMs seriam também de muita utilidade para trabalhar com a IA dos inimigos. Um exemplo de como seria a IA do inimigo Baty, um morcego que persegue o jogador usando algoritmo Pathfinding:
  ![res://gddimg/lvdes.png](gddimg/batyprototipo.png)
  *O morcego inicia em estado Idle, "dormindo" de cabeça para baixo, se a distância entre ele e o Player for menor que seu campo de visão "X" ele entrará em Perseguindo e usará o pathfinding para encontrar o melhor caminho para o ponto onde o Player se localiza. Se atingir o Player, ele irá recuar um pouco: caminhando para o inverso do caminho que faria ao ponto do Pathfinding, enquanto o player está invulnerável, e depois volta a perseguir. Caso o player consiga escapar do campo de visão, ele irá para o estado Voltar e caminhará retornando ao seu ponto inicial. Em todos esses estados ele pode ser atingido e será morto se a vida for menor que 0.*

## Level Design
<center><img src="gddimg/lvdes.png" alt="res://gddimg/lvdes.png" style="zoom:30%;" /></center>
*Mapa do jogo. O jogo é visualmente dividido em "rooms" para o jogador, que delimitam o campo de visão da câmera, como fazem outros jogos do gênero. Os checkpoints na Area E são únicos e ligados à posição em que está o elevador* 

### Area S (Surface)
  - Inimigos: nenhum
  - NPCs: Piggo
  - Boss: DrMolensky
  - Level objects: Carrot
### Area E (Elevador)
  - Inimigos: nenhum
  - NPCs: nenhum
  - Boss: nenhum
  - Level objects: Checkpoint, Elevator, ElevatorControl, Spike
### Area 0 (Tutorial)
  - Inimigos: Crabz, Baty
  - NPCs: nenhum
  - Boss: nenhum
  - Level objects: Spike
### Area 1
  - Inimigos: Crabz (mutations), Baty
  - NPCs: Ratto(2x)
  - Boss: Crabzy (miniboss)
  - Level objects: Orb(2x), MovingPlatform, DestructibleBlock, Spike
### Area 2
  - Inimigos: Squir, Baty
  - NPCs: Ratto(2x), Cowu
  - Boss: Squirr (miniboss)
  - Level objects: Orb(2x), MovingPlatform, DestructibleBlock

<div style="page-break-after: always; break-after: page;"></div>

# Assets

## Artes
Por jrmsrs com auxilio da ferramenta LibreSprite
  - Geral
    - Se basear em dimensoes menores que 16x16
    - Usar apenas cores #FFF e #000 e variações na opacidade (pra se adaptar as cores de fundo)
    - Corpo em #000 e alguns detalhes em #FFF para se destacar em cenarios escuros
  - Personagens
    - Player: coelho
    - Crabz (Crabz, Crabzy)
    - Batty
    - Squir (Squir, Squirr)
    - DrMolensky
    - Ratto (solto, dentro da roda)
    - Piggo
    - Cowu
  - Ambiente
    - Tilesets
    - Level Objects 
      - Elevator
      - Spikes
      - Destructible blocks
      - Moving Platform
    - Itens
      - Orbs
      - Carrot (item, projectile)
      - Nut (projectile)
    - UI
      - Logo
      - Lifebar

## Sons
  Para fim de melhorar a imersão os SFX serão se possível em modo estéreo e volume sofrerá variações em relação a quão proximo ou distante o objeto emitindo som está do Player
  - Music
    - AreaS (superficie): instrumental alegre, remetendo a infancia, talvez sons de animais, baseado em Harvest Moon, Stardew Valley
    - AreaE (elevador): ambient, sintetizadores, eletronico calmo mas baseado um pouco em jazz de elevador
    - Area0 (caverna/tutorial): ambient, sintetizadores, reverberado, um pouco claustrofobico
    - Area1: ambient, sintetizadores, eletronico com batidas calmas
    - Area2: ambient, sintetizadores, um pouco alegre, baseado em Mort Garson - Plantasia
  - Sound Effects
    - Gerais: ataques (hit e miss), explosões suaves
    - Level: bloco destruido (som de terra), engrenagens clockwork
    - Player: passos com som de terra, sons agudos
    - Inimigos
      - Crabz: passos com sons de crack, sons arranhados, inseto
      - Batty: asas batendo, barulhos agudos aleatorios quando não está voando
      - Squir: passos com som de terra, sons de fungadas, medio/agudo
      - minibosses: sons da criatura original com o pitch mais grave
      - DrMolenksy: som de passos, garras, escavadeira, grito de urso
    - NPCs
      - Ratto: feedback quando for salvo, barulhos agudos aleatorios
      - Piggo: passos com som de terra, sons de ronco
      - Cowu: passos com som de terra, sons de mugido

## Animações
Como é demonstrado no diagrama do projeto, cada criatura, sendo Player, NPCs ou inimigos, herdam uma série de atributos das classes entidades e dentre esses atributos os tipos de animações, para se pensar quando for criar as respectivas sprites:

  - Personagens
    - Classe **Creature**: Idle, Move
      - Classe **Grounded**: Jump, Fall
        - Classe **Player**: Climb, Dash, Sliding, Attack1, Attack2, Hurt
          - **Weapon**: Swing
        - Classe **Enemy**: Hurt
          - Classe **Ranged** (Squir, Squirr, etc): Attack
          - Boss
            - **DrMolensky**: Dig, Dash
        - Classe **NPC**: (animações interativas)
  - Outros
    - animações de morte usam sistemas de particulas
    - tomar dano e entrar no estado invulnerável faz a sprite piscar
    - movimentos no chão geram particulas
    - dash gera particula de movimento

Não necessariamente os objetos usam todas as animações que são atribuidas, por exemplo, Squir não possui habilidade de jump e logo não é necessário ter uma animação.

<div style="page-break-after: always; break-after: page;"></div>

# Schedule

  - Primeiros passos, criação das principais sprites
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 21 maio 2022
  - Primeiros contatos com a Game Engine e implementação do jogador e fase
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 21 maio 2022
  - Primeiro inimigo (voador)
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 4 junho 2022
  - Mecânicas iniciais do jogador e ataques (prototipo), camera
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 4 junho 2022
  - Tela inicial (e criação das sprites)
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 6 junho 2022
  - Segundo inimigo (terrestre)
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 24 junho 2022
  - Melhoria nos ataques do jogador e na fase
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 24 junho 2022
  - Cinematica
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 24 junho 2022
  - Polimento na estetica e performance
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 28 junho 2022
  - Checkpoint, progressão, upgrades
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 1 julho 2022
  - Sistema de elevador
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 10 julho 2022
  - Inimigo ranger, minibosses
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 10 julho 2022
  - Polimento nas mecanicas
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 16 julho 2022
  - Implementação de tutorial e tips
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 18 julho 2022
  - Implementação do boss
    - Situação: não começou
    - Time Scale: 30 julho 2022
  - Implementação do final do jogo
    - Situação: <span style="color:limegreen">*concluído*</span>
    - Time Scale: 30 julho 2022
  - Sons do jogo
    - Situação: <span style="color:limegreen">*concluído*</span> but apressado (~~sfx e musicas lindamente roubadas do Minecraft~~)
    - Time Scale: 31 julho 2022

<div style="page-break-after: always; break-after: page;"></div>

# Referências

- [Documentação Godot 3.4 (stable)](https://docs.godotengine.org/en/stable/)  
- [Channel GDQuest](https://www.youtube.com/c/gdquest)  
- [Channel Clécio Espindola GameDev](https://www.youtube.com/c/Cl%C3%A9cioEsp%C3%ADndola)  
- [Channel Fábrica de Jogos - Fabiano Naspolini](https://www.youtube.com/c/fabricaDjogos)  
- Godot A* pathfinding tutorials [[1]](https://www.youtube.com/watch?v=5xoqp9QMKeg) [[2]](https://gist.github.com/athalean/9a2b4e00a72ef102940668c50fdc87a1)  
- [Simple Finite State Machine](https://godottutorials.com/courses/pong-gdscript-series/pong-gdscript-tutorial-04/)  
- Durante a disciplina de At. Curriculares de Extensão: [Curso O Setor de Games (Lúmina | UFRGS)](https://lumina.ufrgs.br/)  
- [Godot Engine - Quick Tips || Tilemap - optimize performance via Visibility Notifier 2D](https://www.youtube.com/watch?v=8QWTpZWPekI&t=24s)  
- [Audio & Music in the Godot Game Engine -- A Belated Tutorial
](https://www.youtube.com/watch?v=nIGJ6ImzSuI)

[[topo](#índice)] [[código-fonte](https://gitlab.com/jrmsrs/coelho-souls)] 